<?php

    Class Acteur extends Personne{

        private array $_roles;
        private array $_castings;

        public function __construct(string $nom, string $prenom, string $dateNaissance,string $sex){
            parent::__construct($nom, $prenom, $dateNaissance,$sex);
            $this->_roles = [];
            $this->_castings = [];
        }

        public function getRoles(): array
        {
                return $this->_roles;
        }
        
        public function addCasting(Casting $casting){
            array_push($this->_castings,$casting);
            array_push($this->_roles,$casting->getRole());
        }

        public function displayRoles()
        {
            
            $roles = " $this has played :";
            foreach($this->_roles as $role){
                $roles .= $role . ",";
            }
            return $roles;

        }

        public function displayFilms(){
            $films = " $this has played in :";
            foreach($this->_castings as $casting){
                $films .= $casting->getFilm() . ",";
            }
            return $films;
        }

        
        
    }



?>