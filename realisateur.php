<?php

    Class Realisateur extends personne{

        private array $_films;

        public function __construct(string $nom, string $prenom, string $dateNaissance,string $sex){
            parent::__construct($nom, $prenom, $dateNaissance,$sex);
            $this->_films = [];
        }

        public function getFilms(): array
        {
                return $this->_films;
        }


        public function addFilm(Film $film){
            array_push($this->_films,$film);
        }

        public function __toString(): string
        {
            $films = "";
            foreach($this->_films as $film){
                $films .= $film->getTitre() . " ";
            }
            return parent::__toString() . " Films : " . $films;
        }

        public function displayFilmography(): string
        {
            $films = "";
            foreach($this->_films as $film){
                $films .= $film->getTitre() . " ";
            }
            return parent::__toString() . " Films : " . $films . ",";
        }

        
    }


?>