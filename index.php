<h1> Cénima Exercice </h1>

<?php

    
    spl_autoload_register(function ($class_name) {
        require $class_name . '.php';
    });

 
    //genres = new Genre("nom");
    $action = new Genre("Action");
    $comedie = new Genre("Comédie");
    $horreur = new Genre("Horreur");

    
    //roles = new Role("nomDeCaractere");
    $hanSolo = new Role("Han Solo");
    $Chewbacca = new Role("Chewbacca");
    $Neo = new Role("Neo");
    $LukeSkywalker = new Role("Luke Skywalker");
    $Morpheus = new Role("Morpheus");
    $AceVentura = new Role("Ace Ventura");
    $Truman = new Role("Truman");
    $TheMask = new Role("The Mask");
    $Batman = new Role ("batman");

    
    //realisateurs = new Realisateur("nom","prenom","dateNaissance","sex");
    $george = new Realisateur("Lucas","Georges","14/05/1944","Homme");
    $matt = new Realisateur("Reeves","Matt","27/04/1966","Homme");
    $tom = new Realisateur("Shadyac","Tom","11/12/1958","Homme");
    $peter = new Realisateur("Weir","Peter","21/08/1944","Homme");
    $chuck = new Realisateur("Russel",'chuck',"30/03/1958","Homme");
    $tim = new Realisateur("Burton","Tim","25/08/1958","Homme");
    $joel = new Realisateur("Schumacher","Joel","29/08/1939","Homme");

    //films = new Film("titre","dateSortie","duree","realisateur","synopsis","genre");
    $starWars = new Film("Star Wars","25/05/1977",121,$george,"Un film de science-fiction",$action);
    $Matrix = new Film("Matrix","23/06/1999",136,$matt,"Un film de science-fiction",$action);
    $aceVentura = new Film("Ace Ventura","04/02/1994",86,$tom,"Un film de comédie",$comedie);
    $trueMan = new Film("The Truman Show","09/06/1998",103,$peter,"Un film de comédie",$comedie);
    $mask  = new Film("The Mask","10/08/1994",101,$chuck,"Un film de comédie",$comedie);
    $batman = new Film("Batman","23/06/1989",136,$tim,"Un film de science-fiction",$action);
    $batman1 = new Film("Batman Returns","19/06/1992",126,$tim,"Un film de science-fiction",$action);
    $batman2 = new Film("Batman Forever","23/06/1995",121,$joel,"Un film de science-fiction",$action);
    $batman3 = new Film("Batman & Robin","23/06/1997",125,$joel,"Un film de science-fiction",$action);
 
    //acteurs = new Acteur("nom","prenom","dateNaissance","sex");
    $ford = new Acteur("Ford","Harrison","13/07/1942","Homme");
    $reeves = new Acteur("Reeves","Keanu","02/09/1964","Homme");
    $hamill = new Acteur("Hamill","Mark","25/09/1951","Homme");
    $fishburne = new Acteur("Fishburne","Laurence","30/07/1961","Homme");
    $carrey = new Acteur("Carrey","Jim","17/01/1962","Homme");
    $affleck = new Acteur("Affleck","Ben","15/08/1972","Homme");
    $clooney = new Acteur("Clooney","George","06/05/1961","Homme");
    $bale = new Acteur("Bale","Christian","30/01/1974","Homme");
    $keaton = new Acteur("Keaton","Michael","05/09/1951","Homme");
    $kilmer = new Acteur("Kilmer","Val","31/12/1959","Homme");


    //$casting = new Casting($acteur,$role,$film);
    $casting = new Casting($ford,$hanSolo,$starWars);
    $casting1 = new Casting($ford,$Chewbacca,$starWars);
    $casting2 = new Casting($reeves,$Neo,$Matrix);
    $casting3 = new Casting($hamill,$LukeSkywalker,$starWars);
    $casting4 = new Casting($fishburne,$Morpheus,$Matrix);
    $casting5 = new Casting($carrey,$AceVentura,$aceVentura);
    $casting6 = new Casting($carrey,$TheMask,$mask);
    $casting7 = new Casting($carrey,$Truman,$trueMan);
    $casting8 = new Casting($keaton,$Batman,$batman);
    $casting9 = new Casting($keaton,$Batman,$batman1);
    $casting10 = new Casting($kilmer,$Batman,$batman2);
    $casting11 = new Casting($clooney,$Batman,$batman3);



    echo $carrey->displayRoles();

    echo "<br>";

    echo $Batman->listeActeurs();
    echo "<br>";
    echo $starWars->listeCasting($casting);
    echo "<br>";
    echo $action->listeFilms();
    echo "<br>";
    echo $carrey->displayFilms();
    echo "<br>";
    echo $george->displayFilmography();
    echo "<br>";
    echo $tim->displayFilmography();
    





    

?>