<?php
    Class Role{
        
        private string $_nomDeCaractere;
        private array $_casting;

        public function __construct(string $nomDeCaractere){
            $this->_nomDeCaractere = $nomDeCaractere;
            $this->_casting = [];

        }

        public function getNomDeCaractere(): string
        {
                return $this->_nomDeCaractere;
        }

        public function setNomDeCaractere(string $_nomDeCaractere): self
        {
                $this->_nomDeCaractere = $_nomDeCaractere;

                return $this;
        }

        public function getCasting(): array
        {
                return $this->_casting;
        }

        public function addCasting(Casting $casting){
            array_push($this->_casting,$casting);
        }


        public function __toString(): string
        {
            return $this->_nomDeCaractere;
        }

        public function listeActeurs(){
            $listeActeurs = [];
            foreach($this->_casting as $casting){
                if($this->_nomDeCaractere == $casting->getRole())
                    {
                        array_push($listeActeurs,$casting->getActeur());
                    }
            }
            $results = "Les acteurs qui jouent $this->_nomDeCaractere sont : ";
            foreach($listeActeurs as $acteur){
                $results .= $acteur->getNom()." ".$acteur->getPrenom().",";
            }
            return $results;
        }

        

        



        
    }


?>