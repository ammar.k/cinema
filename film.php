
<?php

    
    Class Film {
        private string $_titre;
        private string $_dateSortie;
        private int $_duree;
        private Realisateur $_realisateur;
        private string $_synopsis;
        private Genre $_genre;
        private array $_acteurs;
        private array $_castings;


        public function __construct(string $titre, string $dateSortie, int $duree, Realisateur $realisateur, string $synopsis, Genre $genre){
            $this->_titre = $titre;
            $this->_dateSortie = $dateSortie;
            $this->_duree = $duree;
            $this->_realisateur = $realisateur;
            $this->_synopsis = $synopsis;
            $this->_genre = $genre;
            $this->_acteurs = [];
            $this->_castings = [];
            $this->_genre->addFilm($this);
            $this->_realisateur->addFilm($this);

        }


        public function getTitre(): string
        {
                return $this->_titre;
        }

        public function setTitre(string $_titre): self
        {
                $this->_titre = $_titre;

                return $this;
        }

 
        public function getDateSortie(): string
        {
                return $this->_dateSortie;
        }


        public function setDateSortie(string $_dateSortie): self
        {
                $this->_dateSortie = $_dateSortie;

                return $this;
        }

        public function getDuree(): int
        {
                return $this->_duree;
        }

        public function setDuree(int $_duree): self
        {
                $this->_duree = $_duree;

                return $this;
        }

   
        public function getRealisateur(): Realisateur
        {
                return $this->_realisateur;
        }


        public function setRealisateur(string $_realisateur): self
        {
                $this->_realisateur = $_realisateur;

                return $this;
        }

 
        public function getSynopsis(): string
        {
                return $this->_synopsis;
        }


        public function setSynopsis(string $_synopsis): self
        {
                $this->_synopsis = $_synopsis;

                return $this;
        }

        public function getGenre(): Genre
        {
                return $this->_genre;
        }

        public function setGenre(Genre $_genre): self
        {
                $this->_genre = $_genre;

                return $this;
        }

        public function getActeurs(): array
        {
                return $this->_acteurs;
        }

        public function getCastings(): array
        {
                return $this->_castings;
        }

        public function __toString()
        {
                return $this->_titre;
        }


        public function addCasting(Casting $casting)
        {
                array_push($this->_castings,$casting);
                array_push($this->_acteurs,$casting->getActeur());
        }


        public function listeParRole(string $role){
                $liste = "Les acteurs qui ont joué le role de $role dans le film $this->_titre sont : ";
                foreach($this->_castings as $casting){
                        if($casting->getRole()->getNomDeCaractere() == $role){
                                $liste .= $casting->getActeur() . ",";
                        }
                }
                return $liste;
        }

        public function listeCasting(){
                $listeCasting = [];
                foreach($this->_castings as $casting){
                        
                        array_push($listeCasting,$casting->getActeur());
                }
                $results = "Les acteurs qui jouent $this sont : ";
                foreach($listeCasting as $casting){
                        $results .= $casting->getNom()." ".$casting->getPrenom().",";
                }
                return $results;
        }
        


}

    


?>