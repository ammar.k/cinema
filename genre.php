<?php
    Class Genre{
        
        private string $_nom;
        private array $_films;

        public function __construct(string $nom){
            $this->_nom = $nom;
            $this->_films = [];
        }

        public function getNom(): string
        {
                return $this->_nom;
        }

        public function setNom(string $_nom): self
        {
                $this->_nom = $_nom;

                return $this;
        }

        public function __toString(): string
        {
            return $this->_nom;
        }

        public function getFilms(): array
        {
                return $this->_films;
        }

        public function addFilm(Film $film){
            array_push($this->_films,$film);
        }

        public function listeFilms(){
            $listeFilms = [];
            foreach($this->_films as $film){
                if($this->_nom == $film->getGenre())
                    {
                        array_push($listeFilms,$film);
                    }
            }
            $results = "Les films de genre $this->_nom sont : ";
            foreach($listeFilms as $film){
                $results .= $film->getTitre().",";
            }
            return $results;
        }
    }


?>