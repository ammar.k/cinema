<?php
    Class Personne{
        private string $_nom;
        private string $_prenom;
        private string $_dateNaissance;
        private string $_sex;

        public function __construct(string $nom,string $prenom, string $dateNaissance, string $sex ){
            $this->_nom = $nom;
            $this->_prenom = $prenom;
            $this->_dateNaissance = $dateNaissance;
            $this->_sex = $sex;
        }

        public function getNom(): string
        {
                return $this->_nom;
        }


        public function setNom(string $_nom): self
        {
                $this->_nom = $_nom;

                return $this;
        }


        public function getPrenom(): string
        {
                return $this->_prenom;
        }


        public function setPrenom(string $_prenom): self
        {
                $this->_prenom = $_prenom;

                return $this;
        }


        public function getDateNaissance(): string
        {
                return $this->_dateNaissance;
        }


        public function setDateNaissance(string $_dateNaissance): self
        {
                $this->_dateNaissance = $_dateNaissance;

                return $this;
        }

        public function getSex(): string
        {
                return $this->_sex;
        }

 
        public function setSex(string $_sex): self
        {
                $this->_sex = $_sex;

                return $this;
        }

        public function __toString()
        {
                return $this->_nom . " " . $this->_prenom;
        }


        
}

     


?>