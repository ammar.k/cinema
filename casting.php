<?php
    Class Casting{
        
        private Acteur $_acteur;
        private Role $_role;
        private Film $_film;

        public function __construct(Acteur $acteur, Role $role, Film $film){
            $this->_acteur = $acteur;
            $this->_role = $role;
            $this->_film = $film;
            $this->_acteur->addCasting($this);
            $this->_film->addCasting($this);
            $this->_role->addCasting($this);

        }

        public function getActeur(): Acteur
        {
                return $this->_acteur;
        }

        public function setActeur(Acteur $_acteur): self
        {
                $this->_acteur = $_acteur;

                return $this;
        }

        public function getRole(): Role
        {
                return $this->_role;
        }

        public function setRole(Role $_role): self
        {
                $this->_role = $_role;

                return $this;
        }

        public function getFilm(): Film
        {
                return $this->_film;
        }

        public function setFilm(Film $_film): self
        {
                $this->_film = $_film;

                return $this;
        }

        public function __toString(): string
        {
            return $this->_acteur->getNom() . " " . $this->_acteur->getPrenom() . " a joué le role de " . $this->_role->getNomDeCaractere() . " dans le film " . $this->_film->getTitre();
        }

    

}

    


?>